module Data.Route exposing (Route(..), build, fromUrl)

import Url exposing (Url)
import Url.Builder exposing (absolute)
import Url.Parser exposing ((</>), Parser, map, oneOf, parse, s, string, top)


type Route
    = Root
    | Quiz String


fromUrl : Url -> Maybe Route
fromUrl =
    parse <|
        oneOf
            [ map Root top
            , map Quiz (s "q" </> string)
            ]


build : Route -> String
build route =
    case route of
        Root ->
            absolute [] []

        Quiz key ->
            absolute [ "q", key ] []
