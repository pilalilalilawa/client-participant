module Data.AnswerLabel exposing (AnswerLabel(..), encode)

import Json.Encode as Encode


type AnswerLabel
    = A
    | B
    | C
    | D


encode : AnswerLabel -> Encode.Value
encode answerLabel =
    case answerLabel of
        A ->
            Encode.string "A"

        B ->
            Encode.string "B"

        C ->
            Encode.string "C"

        D ->
            Encode.string "D"
