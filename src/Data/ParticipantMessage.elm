module Data.ParticipantMessage exposing (ParticipantMessage(..), encode)

import Data.AnswerLabel as AnswerLabel exposing (AnswerLabel)
import Json.Encode as Encode


type ParticipantMessage
    = RegisterParticipant { key : String, nickname : String }
    | GiveAnswer { questionId : Int, answer : AnswerLabel }


encode : ParticipantMessage -> Encode.Value
encode message =
    case message of
        RegisterParticipant { key, nickname } ->
            Encode.object
                [ ( "tag", Encode.string "RegisterParticipant" )
                , ( "key", Encode.string key )
                , ( "nickname", Encode.string nickname )
                ]

        GiveAnswer { questionId, answer } ->
            Encode.object
                [ ( "tag", Encode.string "GiveAnswer" )
                , ( "questionId", Encode.int questionId )
                , ( "answer", AnswerLabel.encode answer )
                ]
