module Data.ServerMessage exposing (ServerMessage(..), decoder)

import Data.Feedback as Feedback exposing (Feedback)
import Data.Round as Round exposing (Round)
import Json.Decode as Decode exposing (Decoder)


type ServerMessage
    = LobbyWait
    | AskQuestion Round
    | GiveFeedback Feedback


decoder : Decoder ServerMessage
decoder =
    Decode.andThen
        (\tag ->
            case tag of
                "LobbyWait" ->
                    Decode.succeed LobbyWait

                "AskQuestion" ->
                    Decode.map AskQuestion Round.decoder

                "GiveFeedback" ->
                    Decode.map GiveFeedback Feedback.decoder

                _ ->
                    Decode.fail <| "Unknown message type " ++ tag
        )
        (Decode.field "tag" Decode.string)
