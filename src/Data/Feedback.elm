module Data.Feedback exposing (Feedback, decoder)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)


type alias Feedback =
    { correct : Bool
    , moreQuestions : Bool
    }


decoder : Decoder Feedback
decoder =
    Decode.succeed Feedback
        |> required "correct" Decode.bool
        |> required "moreQuestions" Decode.bool
