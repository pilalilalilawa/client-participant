module Data.Round exposing (Round, decoder)

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline exposing (required)


type alias Round =
    { id : Int
    , question : String
    , answers : Answers
    }


type alias Answers =
    { a : String
    , b : String
    , c : String
    , d : String
    }


decoder : Decoder Round
decoder =
    Decode.succeed Round
        |> required "id" Decode.int
        |> required "question" Decode.string
        |> required "answers" answersDecoder


answersDecoder : Decoder Answers
answersDecoder =
    Decode.succeed Answers
        |> required "a" Decode.string
        |> required "b" Decode.string
        |> required "c" Decode.string
        |> required "d" Decode.string
