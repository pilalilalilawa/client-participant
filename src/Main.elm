port module Main exposing (main)

import Browser
import Browser.Events exposing (onAnimationFrameDelta)
import Browser.Navigation as Navigation
import Data.AnswerLabel as AnswerLabel exposing (AnswerLabel(..))
import Data.Feedback exposing (Feedback)
import Data.ParticipantMessage as ParticipantMessage
    exposing
        ( ParticipantMessage(..)
        )
import Data.Round exposing (Round)
import Data.Route as Route
import Data.ServerMessage as ServerMessage exposing (ServerMessage(..))
import Element
    exposing
        ( Color
        , Element
        , alignRight
        , centerX
        , centerY
        , column
        , el
        , fill
        , fillPortion
        , height
        , html
        , htmlAttribute
        , layout
        , link
        , newTabLink
        , none
        , padding
        , paragraph
        , px
        , rgb
        , row
        , shrink
        , spaceEvenly
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input exposing (button)
import Element.Region as Region
import Html exposing (form)
import Html.Attributes as Attributes exposing (id, style, type_, value)
import Html.Events exposing (onClick, onInput, onSubmit)
import Json.Decode as Decode exposing (decodeString, decodeValue)
import Json.Encode as Encode exposing (encode)
import Octicons
import Platform.Cmd exposing (Cmd)
import Platform.Sub exposing (Sub)
import Url exposing (Url)


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = UrlChange
        , onUrlRequest = UrlRequest
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    { quizmasterUrl : String
    , brand : String
    }


type alias Model =
    { state : State
    , navigationKey : Navigation.Key
    , quizmasterUrl : String
    , brand : String
    }


type State
    = Lobby { key : String }
    | Registration { key : String, nickname : String }
    | PendingRegistration
    | Ready
    | QuestionDisplay { round : Round, millisecondsLeft : Float }
    | AnswerGiven
    | FeedbackReceived Feedback
    | PageNotFound


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init { quizmasterUrl, brand } url navigationKey =
    ( { state = navigate url
      , navigationKey = navigationKey
      , quizmasterUrl = quizmasterUrl
      , brand = brand
      }
    , Cmd.none
    )


navigate : Url -> State
navigate url =
    case Route.fromUrl url of
        Just route ->
            case route of
                Route.Root ->
                    Lobby { key = "" }

                Route.Quiz key ->
                    Registration { key = key, nickname = "" }

        Nothing ->
            PageNotFound


type Msg
    = UrlChange Url
    | UrlRequest Browser.UrlRequest
    | ReceiveData String
    | KeyChanged String
    | LobbySubmit
    | NicknameChanged String
    | RegistrationSubmit
    | TimeElapsed Float
    | AnswerSelected AnswerLabel


thinkingTime : Int
thinkingTime =
    20


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( model.state, msg ) of
        ( _, UrlChange url ) ->
            ( { model | state = navigate url }, Cmd.none )

        ( _, UrlRequest request ) ->
            case request of
                Browser.Internal url ->
                    ( model
                    , Navigation.pushUrl
                        model.navigationKey
                        (Url.toString url)
                    )

                Browser.External href ->
                    ( model, Navigation.load href )

        ( _, ReceiveData data ) ->
            case decodeString ServerMessage.decoder data of
                Ok message ->
                    case message of
                        LobbyWait ->
                            ( { model | state = Ready }, Cmd.none )

                        AskQuestion round ->
                            ( { model
                                | state =
                                    QuestionDisplay
                                        { round = round
                                        , millisecondsLeft = toFloat <| thinkingTime * 1000
                                        }
                              }
                            , Cmd.none
                            )

                        GiveFeedback feedback ->
                            ( { model | state = FeedbackReceived feedback }
                            , Cmd.none
                            )

                Err error ->
                    ( model, Cmd.none )

        ( Lobby data, KeyChanged value ) ->
            ( { model | state = Lobby { data | key = value } }
            , Cmd.none
            )

        ( Lobby { key }, LobbySubmit ) ->
            ( model
            , case key of
                "" ->
                    Cmd.none

                _ ->
                    Navigation.pushUrl
                        model.navigationKey
                        (Route.build <| Route.Quiz key)
            )

        ( Registration data, NicknameChanged value ) ->
            ( { model | state = Registration { data | nickname = value } }
            , Cmd.none
            )

        ( Registration { key, nickname }, RegistrationSubmit ) ->
            ( { model | state = PendingRegistration }
            , sendMessage <| RegisterParticipant { key = key, nickname = nickname }
            )

        ( QuestionDisplay state, TimeElapsed delta ) ->
            let
                newMillisecondsLeft =
                    max 0 (state.millisecondsLeft - delta)
            in
            ( { model
                | state =
                    QuestionDisplay
                        { state | millisecondsLeft = newMillisecondsLeft }
              }
            , Cmd.none
            )

        ( QuestionDisplay { round }, AnswerSelected answer ) ->
            ( { model | state = AnswerGiven }
            , sendMessage <|
                GiveAnswer
                    { questionId = round.id
                    , answer = answer
                    }
            )

        _ ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ receiveData ReceiveData
        , case model.state of
            QuestionDisplay { millisecondsLeft } ->
                if millisecondsLeft > 0 then
                    onAnimationFrameDelta TimeElapsed

                else
                    Sub.none

            _ ->
                Sub.none
        ]


type alias Theme =
    { background : Color
    , shading : Color
    , foreground : Color
    }


view : Model -> Browser.Document Msg
view model =
    { title = "Pilalilalilawa!"
    , body = [ layout [] <| ui model ]
    }


ui : Model -> Element Msg
ui model =
    column [ width fill, height fill ]
        [ row
            [ Region.navigation
            , width fill
            , height (px 64)
            , spaceEvenly
            , padding 16
            ]
            [ text model.brand
            , row []
                [ text "powered by "
                , link [ Font.underline ]
                    { url = "https://pilalilalilawa.dev/"
                    , label = text "free software"
                    }
                , text " "
                , html <| Octicons.heart Octicons.defaultOptions
                ]
            ]
        , el [ Region.mainContent, width fill, height fill ] <|
            contentView model
        ]


contentView : Model -> Element Msg
contentView model =
    case model.state of
        Lobby { key } ->
            column [ width fill, height fill ]
                [ el [ centerX, centerY ] <|
                    column [ spacing 8 ]
                        [ html <| form [ onSubmit LobbySubmit, id "lobby-form" ] []
                        , Input.text
                            [ htmlAttribute <| Attributes.form "lobby-form"
                            ]
                            { onChange = KeyChanged
                            , text = key
                            , placeholder = Nothing
                            , label = Input.labelAbove [] (text "Key")
                            }
                        , button
                            [ htmlAttribute <| Attributes.form "lobby-form"
                            , width fill
                            , padding 8
                            , Border.width 1
                            ]
                            { onPress = Just LobbySubmit
                            , label = el [ centerX ] <| text "Enter"
                            }
                        ]
                , row [ height shrink, centerX, padding 16 ]
                    [ text "or "
                    , newTabLink [ Font.underline ]
                        { url = model.quizmasterUrl
                        , label = text "create your own quiz"
                        }
                    ]
                ]

        Registration { nickname } ->
            el [ centerX, centerY ] <|
                column [ spacing 8 ]
                    [ html <|
                        form
                            [ onSubmit RegistrationSubmit
                            , id "registration-form"
                            ]
                            []
                    , Input.username
                        [ htmlAttribute <| Attributes.form "registration-form"
                        ]
                        { onChange = NicknameChanged
                        , text = nickname
                        , placeholder = Nothing
                        , label = Input.labelAbove [] (text "Nickname")
                        }
                    , button
                        [ htmlAttribute <| Attributes.form "registration-form"
                        , width fill
                        , padding 8
                        , Border.width 1
                        ]
                        { onPress = Just RegistrationSubmit
                        , label = el [ centerX ] <| text "Join"
                        }
                    ]

        PendingRegistration ->
            statusMessage "Pending registration..."

        Ready ->
            statusMessage "Ready"

        QuestionDisplay { round, millisecondsLeft } ->
            el [ width fill, height fill, padding 8 ] <|
                column [ width fill, height fill, spacing 8 ]
                    [ html <| form [ id "answers-form" ] []
                    , el [ width fill, height fill, Border.width 1 ] <|
                        paragraph
                            [ centerX
                            , centerY
                            , padding 16
                            , htmlAttribute <| style "text-align" "center"
                            ]
                            [ text round.question ]
                    , column [ width fill, height fill, spacing 8 ]
                        [ row [ width fill, height fill, spacing 8 ]
                            [ questionButton ( A, round.answers.a )
                            , questionButton ( B, round.answers.b )
                            ]
                        , row [ width fill, height fill, spacing 8 ]
                            [ questionButton ( C, round.answers.c )
                            , questionButton ( D, round.answers.d )
                            ]
                        ]
                    , el [ width fill, height (px 16) ] <|
                        let
                            percentage =
                                1.0 - millisecondsLeft / toFloat (thinkingTime * 1000)

                            units =
                                1000000.0
                        in
                        row [ width fill, Border.width 1 ]
                            [ el
                                [ width
                                    (fillPortion
                                        (Basics.round <| percentage * units)
                                    )
                                , height (px 16)
                                , Background.color (rgb 0 0 0)
                                ]
                                none
                            , el
                                [ width
                                    (fillPortion
                                        (Basics.round <| (1 - percentage) * units)
                                    )
                                , height (px 16)
                                ]
                                none
                            ]
                    ]

        AnswerGiven ->
            statusMessage "Waiting for the other players..."

        FeedbackReceived feedback ->
            el [ centerX, centerY ] <|
                column [ spacing 16 ]
                    [ el [ centerX ] <|
                        text <|
                            if feedback.correct then
                                "Correct!"

                            else
                                "Wrong!"
                    , if feedback.moreQuestions then
                        none

                      else
                        text "The quiz is over. Thanks for participating!"
                    ]

        PageNotFound ->
            statusMessage "Page not found."


questionButton : ( AnswerLabel, String ) -> Element Msg
questionButton ( answerLabel, answerText ) =
    button
        [ htmlAttribute <| Attributes.form "answers-form"
        , width fill
        , height fill
        , Border.width 1
        ]
        { onPress = Just (AnswerSelected answerLabel)
        , label =
                paragraph
                    [ centerX
                    , centerY
                    , padding 16
                    , htmlAttribute <| style "text-align" "center"
                    ]
                    [ text answerText ]
        }


statusMessage : String -> Element Msg
statusMessage message =
    el [ centerX, centerY ] (text message)


sendMessage : ParticipantMessage -> Cmd Msg
sendMessage =
    sendData << encode 0 << ParticipantMessage.encode


port sendData : String -> Cmd msg


port receiveData : (String -> msg) -> Sub msg
